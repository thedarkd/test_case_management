@extends('layouts.app')

@section('content')
    <div class="card" style="width: 100%;">
        <div class="card">
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="card-header">
                        <h3>[{{$data->id}}] Title:</h3>
                    </div>
                    <br>
                    <h4 style="margin-left: 30px;">{{$data->title}}</h4>
                </li>
                <li class="list-group-item">
                    <div class="card-header">
                        <h3>Description/Summary:</h3>
                    </div>
                    <br>
                    <h4 style="margin-left: 30px;">{{$data->description}}</h4>
                </li>

                <li class="list-group-item">
                    <div class="card-header">
                        <h3>Pre-condition <small>(Any requirement that needs to be done before execution of this test case. To execute this test case list all pre-conditions)</small>:</h3>
                    </div>
                    <br>
                    <h4>{{$data->pre_condition}}</h4>
                </li>

                <li class="list-group-item">
                    <div class="card-header">
                        <h3>Dependencies <small>(Determine any dependencies on test requirements or other test cases)</small>:</h3>
                    </div>
                    <br>
                    <h4 style="margin-left: 30px;">{{$data->dependencies}}</h4>
                </li>

                <li class="list-group-item">
                    <div class="card-header">
                        <h3>Test Steps:
                            <small>(Mention all the test steps in detail and write in the order in which it requires to be executed. While writing test steps ensure that you provide as much detail as you can)</small>:</h3>
                    </div>
                    <br>
                    <h4 style="margin-left: 30px;">{{$data->test_steps}}</h4>
                </li>

                <li class="list-group-item">
                    <div class="card-header">
                        <h3>Test Data: <small>(Use of test data as an input for the test case. Deliver different data sets with precise values to be used as an input)</small>:</h3>
                    </div>
                    <br>
                    <h4 style="margin-left: 30px;">{{$data->test_data}}</h4>
                </li>

                <li class="list-group-item">
                    <div class="card-header">
                        <h3>Expected Results:
                            <small>()</small>:</h3>
                    </div>
                    <br>
                    <h4 style="margin-left: 30px;">{{$data->expected_results}}</h4>
                </li>
                <li class="list-group-item">
                    <div class="card-header">
                        <h3>Post-Condition:
                            <small>(What would be the state of the system after running the test case? )</small>:</h3>
                    </div>
                    <br>
                    <h4 style="margin-left: 30px;">{{$data->post_condition}}</h4>
                </li>

                <li class="list-group-item">
                    <div class="card-header">
                        <h3>Actual Result:
                            <small>()</small>:</h3>
                    </div>
                    <br>
                    <h4 style="margin-left: 30px;">{{$data->actual_result}}</>
                </li>

                <li class="list-group-item">
                    <div class="card-header">
                        <h3>Status (Fail/Pass): <small>()</small>:</h3>
                    </div>
                    <br>
                    <h4 style="margin-left: 30px;">{{$data->status}}</h4>
                </li>

                <li class="list-group-item">
                    <div class="card-header">
                        <h3>Notes <small>()</small>:</h3>
                    </div>
                    <br>
                    <h4 style="margin-left: 30px;">{{$data->notes}}</h4>
                </li>
            </ul>
        </div>
    </div>
@endsection