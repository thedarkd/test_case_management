@extends('layouts.app')

@section('content')
    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Description</th>
        </tr>
        </thead>
        <tbody>

        @foreach($data as $suite)
        <tr>
            <td>{{$suite->id}}</td>
            <td><a href="index.php/testsuites/{{$suite->id}}">{{$suite->title}}</a></td>
            <td>{{$suite->description}}</td>
        </tr>
    @endforeach
        </tbody>
    </table>
@endsection