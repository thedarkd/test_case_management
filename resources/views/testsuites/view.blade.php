@extends('layouts.app')

@section('content')
    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Description</th>
            <th scope="col">Status</th>
        </tr>
        </thead>
        <tbody>

        @foreach($data as $testcase)
        <tr>
            <td>{{$testcase->id}}</td>
            <td><a href="../testcases/{{$testcase->id}}">{{$testcase->title}}</a></td>
            <td>{{$testcase->description}}</td>
            <td>{{$testcase->status}}</td>
        </tr>
    @endforeach
        </tbody>
    </table>
@endsection