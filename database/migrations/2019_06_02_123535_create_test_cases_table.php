<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_cases', function (Blueprint $table) {
            $table->bigIncrements('id');
	        $table->string('title');
	        $table->integer('created_by');
	        $table->integer('test_suite_id');
	        $table->string('priority');
	        $table->text('pre_condition');
	        $table->text('dependencies');
	        $table->text('test_steps');
	        $table->text('test_data');
	        $table->text('expected_results');
	        $table->text('post_condition');
	        $table->text('actual_result');
	        $table->string('status');
	        $table->text('description');
	        $table->text('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_cases');
    }
}
