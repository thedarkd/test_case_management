<?php

namespace App\Http\Controllers;

use App\TestCase;
use App\TestSuite;
use Illuminate\Http\Request;

class TestSuites extends Controller
{
    //
	public function index(){
		$data = TestSuite::all();
		return view('testsuites.index')->with('data', $data);
	}

	public function view($id){
		$data = TestCase::where('test_suite_id', $id)->get();
		return view('testsuites.view')->with('data', $data);
	}
}
