<?php

namespace App\Http\Controllers;

use App\TestCase;
use Illuminate\Http\Request;

class TestCases extends Controller
{
    //
	public function index(){
		$data = TestCase::all();
		return view('testcases.index')->with('data', $data);
	}

	public function view($id){
		$data = TestCase::where('id', $id)->first();
		return view('testcases.view')->with('data', $data);
	}
}
